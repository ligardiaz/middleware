<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

Route::get('/', function () {
    return view('index');
})->name('index');
Route::get('/form', function () {
    return view('form');
})->name('form');
Route::post('/welcome', function () {
    return view('welcome');
})->name('welcome');
Route::get('/data-tables', function () {
    return view ('data-tables');
})->name('data-tables');

//Router Middleware
Route::middleware(['auth'])->group(function () {

//Router Tugas CRUD
Route::get('/cast', [CastController::class, 'index'])->name('cast.index');
Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');
Route::post('/cast/store', [CastController::class, 'store'])->name('cast.store');
Route::get('/cast/{cast_id}/show', [CastController::class, 'show'])->name('cast.show');
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('cast.edit');
Route::put('/cast/{cast_id}/update', [CastController::class, 'update'])->name('cast.update');
Route::delete('/cast/{cast_id}/delete', [CastController::class, 'delete'])->name('cast.delete');

});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
